# LATITUDE

## Sobre nós:
Criado em 2010, com apoio financeiro da Dell, o Laboratório de Tecnologias da Tomada de Decisão (LATITUDE.UnB) articula competências nas áreas de engenharia de redes, sistemas distribuídos, engenharia de software, busca e indexação, arquitetura da informação, exploração multidimensional da informação e arquitetura de sistemas de informação, para aplicação ao suporte e ajuda à tomada de decisão, à inteligência nos negócios e comportamentos organizacionais (business intelligence) e à gestão com base em tecnologias da informação.


## O que estamos procurando:
Estamos procurando por alunos da UnB que estejam no meio da sua graduação que sejam apaixonados por programação e por novos desafios, e que estejam interessado sem participar dos projetos do laboratorio LATITUDE.

Estamos procurando alguem que:

 - Esteja em constante processo de aprendizagem (Busque conhecimento)
 - Tenha sólidos conhecimentos em computação
 - Saiba trabalhar em equipe, aprendendo e ensinando
 - Tenha experiência em Desenvolvimento Web
 - Saiba usar Git e ambiente Unix

## Nossa filosofia
Temos a filosofia de manter a tecnologia do jeito mais simples e elegante possível, todo o nosso stack de desenvolvimento reflete isso. Buscamos qualidade de código e da arquitetura, sempre deixando nossos testes em dia.  Acreditamos que o desejo contínuo de aprender é a característica mais importante em qualquer profissional, estamos sempre descobrindo novas tecnologias que resolvam os problemas de um jeito melhor. 

## Nosso Stack:

1. Front-end
- HTML
- CSS
- Angular
- Ionic

2. Back-End
- Python
- Java (Spring Boot)
- Node

3. Demais tecnologias
- Linux
- Docker
- Banco de dados Relacional e não Relacional.

## Atrativos
 - Ambiente de trabalho informal (por enquanto estamos todos home office)
 - Horario flexível, pois sabemos que todos tem seus horarios de aula 
 - Liberdade de escolha em seu ambiente de desenvolvimento
 - Escrever artigos, sempre incetivamos a escrita de artigos científicos

# Proposta

A proposta deste desafio é fazer um cadastro de pessoas com covid-19, onde a pessoa irá entrar no site, e cadastrar suas informaçoes e poderá ver as informações de todos os outros que se cadastraram, para ver se a pessoa mora perto.

O desenvolvimento incluirá a construção do Front-End e do Back-End

Deverá então ser desenvolvida uma aplicação SPA [Angular preferencialemente]  que consuma uma API local em qualquer linguagem [Java Spring preferencialmente] e que guarde as informações em um banco de dados [Postgres preferencialemente]. 

A aplicação terá:

1. Front-end. que contenha pelomenos:
- Uma tela de cadastro (o cadastro da pessoa, contendo pelomenos Nome e endereço)
- Uma tela de para verificar as pessoas cadastradas (um tabelão, com as informações dos cadastros)

2. Back-end. Uma api rest que tenha pelomenos:
- Um POST para o cadastro 
- Um GET para buscar os usuários cadastrados

#### Pontos mínimos necessários
- Documentação (de preferência em inglês)

#### Bônus

- Validações dos campos
- Máscara nos campos
- Mostrar um ponto no mapa do googlemaps (ou outro), com o endereço cadastrado
- Criação de um Docker-compose para subir as duas aplicações no docker
- Uso da [Integração Continua](https://about.gitlab.com/features/gitlab-ci-cd/) do Gitlab para a criação de um contêiner válido no 
*registry* do repositório.
- Um tipo de teste automatizado para validar o CI.

### Termos de entrega

A entrega da proposta pode ser feita a partir de um *Merge Request* neste repositório.
Também inclua seu CV com informação de contato como um arquivo do projeto.

Voce deverá criar uma *branch* com o seu [nome.sobrenome] e submeter seu *Merge Request*